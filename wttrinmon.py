#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: msans
"""
import sys
import argparse
import pathlib
#from datetime import datetime
from datetime import date
import requests
import tomli
import json


class WttrInMonitor:
    """WttrIn inverter monitoring class"""
    def __init__(self, user_data_file:str, data_storage_folder: str):

        self.err = False
        self.err_msg = ''

        self.data_storage_folder = data_storage_folder  # FIXME raise an error if no such folder

        self.today = date.today()

        self.location = ''
        self._get_user_data(user_data_file)
        self.url = f"https://wttr.in/{self.location}?format=j1"


    def _get_user_data(self, user_data_file: str) -> dict:
        """Get user data (site location, etc.) from a toml file"""
        p = pathlib.Path(user_data_file)
        p = p.expanduser()
        p = p.resolve()
        user_data = dict()

        try:
            with open(p, 'rb') as f:
                user_data = tomli.load(f)
        except (IOError, tomli.TOMLDecodeError) as err:
            self.err_msg = f"ERROR: could not read config file {p}: " + str(err)
            self.err = True
            return user_data

        try:
            self.location = user_data["location"]
        except (KeyError) as err:
            self.err_msg = f"ERROR: reading config file {p}: " + str(err)
            self.err = True
            return user_data
        return user_data


    def get_data(self, refresh_data: bool, max_fetch_attempt: str, quiet: bool) -> bool:

        p = pathlib.Path(pathlib.Path(self.data_storage_folder), pathlib.Path(str(self.today) + ".json"))

        # skip existing valid data file if no refresh required
        # if not refresh_data and p.is_file():
        #     if not (quiet):
        #         sys.stdout.write(f"Fetching {d} data ... SKIPPED (no refresh required)\n")


        # get and format raw data
        #fetch_count_max = int(max_fetch_attempt)
        #fetch_count = fetch_count_max
        # while fetch_count > 0:
        #     if not(quiet):
        #         sys.stdout.write(
        #             f"Fetching {d} data (attempt {fetch_count_max-fetch_count+1}/{fetch_count_max}) ...")
        #     break
        #
        # if fetch_count == 0:
        #     print("ERROR: solaredge server is returning NaN data. Aborting.")
        #     return True

        response = requests.get(self.url)
        try:
            response_dict = response.json()
        except json.JSONDecodeError as err:
            self.err = True
            self.err_msg = "ERROR: could not decode json response: " + str(err)
            return True

        # dump
        try:
            with open(p, "w") as outfile:
                json.dump(response_dict, outfile)
        except IOError as err:
            self.err_msg = f"ERROR: could not write {p}:" + str(err)
            self.err = True
            return True
        return self.err

        try:
            response_dict.to_json(p)
        except IOError as err:
            self.err_msg = f"ERROR: could not write {p}:" + str(err)
            self.err = True
            return True

        return False

    def is_error(self):
        return self.err

    def get_error(self):
        return self.err_msg

def meteostatmon() -> int:
    """Monitor meteostat data"""

    # argument parsing
    script_path = pathlib.Path(__file__).absolute()
    usage = "%(prog)s [options] [USERDATAFILE]"
    description = script_path.name + " is a wrapper for meteostat module"
    parser = argparse.ArgumentParser(usage=usage, description=description)

    parser.add_argument("-q", "--quiet", dest="quiet", action='store_true',
                        help="Quiet mode.")
    parser.add_argument("-o", "--output-folder", dest="output_folder", action='store', default='.', type=str,
                        help="Path to the output folder for the json data files")
    parser.add_argument("-R", "--refresh-data", dest = "refresh_data", action = 'store_true',
                        help="Fetch data even if data file exists.")
    parser.add_argument("-m", "--max-fetch-attempt", dest="max_fetch_attempt", default="20", action='store', type=int,
                        help="Maximum number of data fetch attempts.")
    parser.add_argument("user_data_file",
                        action="store", default='wttrin_userdata.toml', nargs="?", metavar="USERDATAFILE",
                        help="Path to the file (TOML) containing the user data .")

    args = parser.parse_args()

    # argument post-processing
    args.max_fetch_attempt = str(max(1,int(args.max_fetch_attempt)))

    # init wttrin
    mon = WttrInMonitor(args.user_data_file, args.output_folder)
    if mon.is_error():
        print(mon.get_error())
        return 1

    mon.get_data(args.refresh_data, args.max_fetch_attempt, args.quiet)
    if mon.is_error():
        print(mon.get_error())
        return 1

    return 0

if __name__ == '__main__':
    sys.exit(meteostatmon())
