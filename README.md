# wttrinmon

Command line tools for fetching local meteo forecasting data from wttrin servers.

## Setup

* The [wttr.in](https://github.com/chubin/wttr.in) github page
* The [requests](https://3.python-requests.org/) project page

Installation via `pip` in a virtual environment:

    pip install requests tomli

## Usage

    usage: wttrinmon.py [options] [USERDATAFILE]

    wttrinmon.py is a wrapper for meteostat module
    
    positional arguments:
      USERDATAFILE          Path to the file (TOML) containing the user data .
    
    options:
      -h, --help            show this help message and exit
      -q, --quiet           Quiet mode.
      -o OUTPUT_FOLDER, --output-folder OUTPUT_FOLDER
                            Path to the output folder for the json data files
      -R, --refresh-data    Fetch data even if data file exists.
      -m MAX_FETCH_ATTEMPT, --max-fetch-attempt MAX_FETCH_ATTEMPT
                            Maximum number of data fetch attempts.
